const getAllReviewMiddleware = (req, res, next) => {
    console.log("GET all review middleware");

    next();
}

const createReviewMiddleware = (req, res, next) => {
    console.log("POST review middleware");

    next();
}

const getReviewByIDMiddleware = (req, res, next) => {
    console.log("GET review by id middleware");

    next();
}

const updateReviewMiddleware = (req, res, next) => {
    console.log("PUT review middleware");

    next();
}

const deleteReviewMiddleware = (req, res, next) => {
    console.log("DELETE review middleware");

    next();
}

module.exports = {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIDMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}