const express = require("express");

const router = express.Router();

const {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIDMiddleware,
    deleteReviewMiddleware,
    updateReviewMiddleware
} = require("../middlewares/review.middleware");

const {
    createReview,
    getAllReviews,
    getReviewById,
    updateReview,
    deleteReview
} = require("../controllers/review.controller");

router.get("/", getAllReviewMiddleware, getAllReviews);

router.post("/", createReviewMiddleware, createReview);

router.get("/:reviewId", getReviewByIDMiddleware, getReviewById);

router.put("/:reviewId", updateReviewMiddleware, updateReview);

router.delete("/:reviewId", deleteReviewMiddleware, deleteReview);

module.exports = router;