const express = require("express");

const router = express.Router();

const {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getCourseByIDMiddleware,
    deleteCourseMiddleware,
    updateCourseMiddleware
} = require("../middlewares/course.middleware");

const {
    createCourse,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
} = require("../controllers/course.controller");

router.get("/", getAllCourseMiddleware, getAllCourses);

router.post("/", createCourseMiddleware, createCourse);

router.get("/:courseid", getCourseByIDMiddleware, getCourseById);

router.put("/:courseid", updateCourseMiddleware, updateCourseById);

router.delete("/:courseid", deleteCourseMiddleware, deleteCourseById);

module.exports = router;