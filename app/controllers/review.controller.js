//import thư viện mongoose
const mongoose = require('mongoose');

// import review model
const reviewModel = require('../models/review.model');

// import course model
const courseModel = require('../models/course.model');

// const create review 
const createReview = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        courseId,
        stars,
        note
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Course ID is not valid"
        })
    }

    // Star là số nguyên, lớn hơn 0 và nhỏ hơn 6
    // Number.isInteger(stars) && stars > 0 && stars < 6
    if (stars === undefined || !(Number.isInteger(stars) && stars > 0 && stars < 6)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Star is not valid"
        })
    }

    // B3: Thao tác với CSDL
    var newReview = {
        stars: stars,
        note: note
    }

    try {
        const createdReview = await reviewModel.create(newReview);

        const updatedCourse = await courseModel.findByIdAndUpdate(courseId, {
            $push: { reviews: createdReview._id }
        })

        return res.status(201).json({
            status: "Create review successfully",
            course: updatedCourse,
            data: createdReview
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllReviews = async (req, res) => {
    //B1: thu thập dữ liệu
    const courseId = req.query.courseId;
    //B2: kiểm tra
    if (courseId !== undefined && !mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Course ID is not valid"
        })
    }

    //B3: thực thi model
    try {
        if(courseId === undefined) {
            const reviewList = await reviewModel.find();

            if(reviewList && reviewList.length > 0) {
                return res.status(200).json({
                    status: "Get all reviews sucessfully",
                    data: reviewList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any review"
                })
            }
        } else {
            const courseInfo = await courseModel.findById(courseId).populate("reviews");

            return res.status(200).json({
                status: "Get all reviews of course sucessfully",
                data: courseInfo.reviews
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getReviewById = async (req, res) => {
    //B1: thu thập dữ liệu
    var reviewId = req.params.reviewId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const reviewInfo = await reviewModel.findById(reviewId);

        if (reviewInfo) {
            return res.status(200).json({
                status:"Get review by id sucessfully",
                data: reviewInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateReview = async (req, res) => {
    //B1: thu thập dữ liệu
    var reviewId = req.params.reviewId;

    const {stars, note} = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    // Nếu stars là undefined => vẫn hợp lệ
    // Nếu stars khác undefined và không thỏa mãn điều kiện hợp lệ => return lỗi 
    if (stars !== undefined && !(Number.isInteger(stars) && stars > 0 && stars < 6)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Star is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updateReview = {
            stars,
            note
        }

        const updatedReview = await reviewModel.findByIdAndUpdate(reviewId, updateReview);

        if (updatedReview) {
            return res.status(200).json({
                status:"Update review sucessfully",
                data: updatedReview
            })
        } else {
            return res.status(404).json({
                status:"Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteReview = async (req, res) => {
    //B1: Thu thập dữ liệu
    var reviewId = req.params.reviewId;
    // Nếu muốn xóa id thừa trong mảng review thì có thể truyền thêm coureId để xóa
    var courseId = req.query.courseId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedReview = await reviewModel.findByIdAndDelete(reviewId);

        // Nếu có courseId thì xóa thêm (optional)
        if(courseId !== undefined) {
            await courseModel.findByIdAndUpdate(courseId, {
                $pull: { reviews: reviewId }
            })
        }

        if (deletedReview) {
            return res.status(200).json({
                status:"Delete review sucessfully",
                data: deletedReview
            })
        } else {
            return res.status(404).json({
                status:"Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    createReview,
    getAllReviews,
    getReviewById,
    updateReview,
    deleteReview
}