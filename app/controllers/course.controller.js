const courseModel = require("../models/course.model");
const mongoose = require("mongoose");

const createCourse = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        title,
        description,
        noStudent
    } = req.body;

    // console.log(req.body);
    // { reqTitle: 'R46', reqDescription: 'React & NodeJS', reqStudent: 20 }

    // B2: Validate du lieu
    if (!title) {
        return res.status(400).json({
            message: "Title khong hop le"
        })
    }

    if (noStudent < 0) {
        return res.status(400).json({
            message: "So hoc sinh khong hop le"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newCourse = {
            title: title,
            description: description,
            noStudent: noStudent
        }

        const result = await courseModel.create(newCourse);

        return res.status(201).json({
            message: "Tao course thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error

        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllCourses = async (req, res) => {
    // B1: Thu thap du lieu
    let courseName = req.query.courseName;
    let minStudent = req.query.minStudent;
    let maxStudent = req.query.maxStudent;

    //tạo ra điều kiện lọc
    let condition = {};

    if (courseName) {
        //condition.title = courseName;        
        condition.title = courseName;        
    }

    if (minStudent) {
        condition.noStudent = { $gte : minStudent};
    }

    if (maxStudent) {
        condition.noStudent = { ...condition.noStudent, $lte : maxStudent};
    }

    // let condition2 = {
    //     title : courseName,
    //     $or : [
    //         { noStudent : { $lt : minStudent}},
    //         { noStudent : { $gt : maxStudent}}
    //     ]
    // }
    // console.log(condition);

    // //B2: Validate dữ liệu
    // //B3: Thao tác với cơ sở dữ liệu
    // courseModel.find(condition)
    //     .sort({title:'desc'})
    //     .skip(3)
    //     //.limit(1)        
    //     .exec((error, data) => {
    //         if(error) {
    //             return response.status(500).json({
    //                 status: "Error 500: Internal server error",
    //                 message: error.message
    //             })
    //         } else {
    //             return response.status(200).json({
    //                 status: "Success: Get courses success",
    //                 data: data
    //             })
    //         }
    //     });
    // /*
    // courseModel.find(condition, {_id:0, title:1, description:1}, (error, data) => {
    //     if(error) {
    //         return response.status(500).json({
    //             status: "Error 500: Internal server error",
    //             message: error.message
    //         })
    //     } else {
    //         return response.status(200).json({
    //             status: "Success: Get courses success",
    //             data: data
    //         })
    //     }
    // })*/

    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await courseModel.find(condition);

        return res.status(200).json({
            message: "Lay danh sach course thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findById(courseid);

        if (result) {
            return res.status(200).json({
                message: "Lay thong tin course thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;
    const {
        title,
        description,
        noStudent
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    if (title === "") {
        return res.status(400).json({
            message: "Title khong hop le"
        })
    }

    if (noStudent < 0) {
        return res.status(400).json({
            message: "So hoc sinh khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateCourse = {};
        if (title) {
            newUpdateCourse.title = title
        }
        if (description) {
            newUpdateCourse.description = description
        }
        if (noStudent) {
            newUpdateCourse.noStudent = noStudent
        }

        const result = await courseModel.findByIdAndUpdate(courseid, newUpdateCourse);

        if (result) {
            const finalResult = await courseModel.findById(courseid);
            return res.status(200).json({
                message: "Update thong tin course thanh cong",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findByIdAndRemove(courseid);

        if (result) {
            return res.status(200).json({
                message: "Xoa thong tin course thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

module.exports = {
    createCourse,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
}
