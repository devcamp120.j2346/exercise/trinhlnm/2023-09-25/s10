const mongoose = require("mongoose");

const Shema = mongoose.Schema;

const reviewSchema = new Shema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Review", reviewSchema);

