const expect = require("chai").expect;
const request = require("supertest");
const Course = require("../app/models/course.model");
const app = require("../index");
const mongoose = require('mongoose');
const env = process.env.NODE_ENV || 'development';

describe("/api/v1/courses", () => {
    before(async () => {
        await Course.deleteMany({});
    });

    after(async () => {
        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/R46_Course_Review");
    });

    describe("GET /", () => {
        it("should return all course", async () => {
            const courses = [
                { title: "title 1", description: "description 1", noStudent: 30 },
                { title: "title 2", description: "description 2", noStudent: 32 },
            ];
            const result = await Course.insertMany(courses);
            const res = await request(app).get("/api/v1/courses");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET/:id", () => {
        it("should return a Course if valid id is passed", async () => {
            const course = new Course({
                title: "title 3", description: "description 3", noStudent: 32 
            });
            await course.save();
            const res = await request(app).get("/api/v1/courses/" + course._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("title", course.title);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/courses/sadf");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/courses/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });


    describe("POST /", () => {
        it("should return Course when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/courses")
                .send({
                    title: "title 4", description: "description 4", noStudent: 30
                });
            const data = res.body.data;
            expect(res.status).to.equal(201);
            expect(data).to.have.property("_id");
            expect(data).to.have.property("description", "description 4");
            expect(data).to.have.property("noStudent", 30);

            const course = await Course.findOne({ _id: data._id });
            expect(course.title).to.equal('title 4');
            expect(course.description).to.equal('description 4');
        });
    });

    describe("PUT /:id", () => {
        it("should update the existing Course and return 200", async () => {
            const course = new Course({
                title: "title 5", description: "description 5", noStudent: 34
            });
            await course.save();

            const res = await request(app)
                .put("/api/v1/courses/" + course._id)
                .send({
                    title: "title 55",
                    description: "description 55",
                });

            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("title", "title 55");
            expect(res.body.data).to.have.property("description", "description 55");
        });
    });

    describe("DELETE /:id", () => {
        var id = "";
        const course = new Course({
            title: "title 6", description: "description 6", noStudent: 33
        });

        it("should delete requested id and return response 200", async () => {
            await course.save();
            id = course._id;
            const res = await request(app).delete("/api/v1/courses/" + id);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted Course is requested", async () => {
            let res = await request(app).get("/api/v1/courses/" + id);
            expect(res.status).to.be.equal(404);
        });
    });
});

